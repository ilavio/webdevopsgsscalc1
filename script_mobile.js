//@ts-check

let pressure = 0;
let pressureMove = 0;
let bal = 1;
let rez = 55;
let expense = 40;
let volumeBal = 7;
let minutesForMove = 0;
let datePointEntr;
let datePointReached;

//variable from html

let messageMagazine = "";
let magazineWorck;

/**
 * function for preliminary calculation pressure exit
 * from the polluted zone
 * @param {Object} el 
 * @returns {boolean}
 */
function preliminaryСalculation(el) {
    magazineWorck = document.getElementById('magazine');

    document.getElementById('error').innerHTML = "";

    messageMagazine = (getDateNow() + "Давление выхода составляет: \n" + getPressureExit(el.pressure.value)
        + "мПа при давлении: " + el.pressure.value + "мПа");

    console.log(messageMagazine);
    magazineWorck.innerHTML = messageMagazine;

    return false;
}

/**
 * function get pressure after reconnaissance 
 * @param {Object} el 
 * @returns {boolean}
 */
function calculationPressureExit(el) {
    document.getElementById('error').innerHTML = "";
    let pressureMoveX = el.pressure_move.value;
    let pressureExit = getPressureMoveExit(pressureMoveX);
    let timeForWorck = getTimeWorckAfterMove(pressureExit);

    messageMagazine = (magazineWorck.value + "\n" + getDateNow() + 
    `Давление выхода составляет: ${pressureExit} мПа\n` +
        `при расходе давления на разведку: ${pressureMove} мПа\n` + 
        `Время для работ: ${timeForWorck}мин.`);


    console.log(messageMagazine);
    magazineWorck.innerHTML = messageMagazine;

    return false;
}

/**
 * function get Pressure Exit
 * @param {Object} pressureX 
 * @returns {Number}
 */
function getPressureExit(pressureX) {
    let pressureExit = 0;

    try {
        pressure = checkNumber(pressureX);

        pressureExit = Math.round(((pressure - rez) / 3) * 2 + rez);
        return pressureExit;
    } catch (ex) {
        document.getElementById('error').innerHTML = ex.name + "; " + ex.message;
        console.log(ex.name + "; " + ex.message);
    }

}

/**
 * 
 * @returns {boolean}
 */
function calculationTimeForReconnaissance() {
    datePointEntr = new Date();

    messageMagazine = (magazineWorck.value + "\n" + `${getDateNow()}` +
        `Общее время: ${getTimeGeneral()} мин.\n Время для движения: ${minutesForMove} мин.`);

    magazineWorck.innerHTML = messageMagazine;

    return false
}

function calculationTimeReached() {
    datePointReached = new Date();
    datePointReached.setMinutes(datePointReached.getMinutes() + 3); // установка времени на 3 мин вперед для проверки;
    console.log("Время перемотано на 3 мин:  -->" + datePointReached);
    let timeGeneral = getTimeGeneral();
    minutesForMove = datePointReached.getMinutes() - datePointEntr.getMinutes();
    let timeWorck = timeGeneral - (3 * minutesForMove);

    messageMagazine = (magazineWorck.value + `\n ${getDateNow()}` + 
    `Достигли точки за: ${minutesForMove}мин.\n`+
    `Время для проведения работ: ${timeWorck}мин`);
    magazineWorck.innerHTML = messageMagazine;

    return false
}



/**
 * 
 * @param {String} pressureMoveX 
 * @returns {Number}
 */
function getPressureMoveExit(pressureMoveX) {
    try {
        let x = document.getElementById("pressure").value;
        pressure = checkNumber(x);
        pressureMove = checkNumber(pressureMoveX);
        pressureMove = pressure - pressureMove;

        return Math.round((2 * pressureMove + rez));

    } catch (ex) {
        document.getElementById('error').innerHTML = ex.name + "; " + ex.message;
        console.log(ex.name + "; " + ex.message);
    }
}

/**
 * function get time general
 * @returns {Number}
 */
function getTimeGeneral() {
    let tOb = 0;

    tOb = ((pressure - rez) * bal * volumeBal) / expense;
    minutesForMove = Math.round(tOb / 3);
    return Math.round(tOb);
}

/**
 * function get time after move forward
 * @param {Number} pressureExit 
 * @returns {Number}
 */
function getTimeWorckAfterMove(pressureExit) {
    minutesForMove = ((pressure - (pressureMove + pressureExit)) * volumeBal * bal) / expense;
    
    return  Math.round(minutesForMove);
}

/**
 * function get date in format dd.mm.gg - hh:mm
 * @returns {String}  
 */
function getDateNow() {
    let dateNow = new Date();
    let year = dateNow.getFullYear();
    let month;
    let day;
    let hour;
    let minets;

    if (dateNow.getMonth() < 10) {
        month = "0" + (dateNow.getMonth() + 1);
    } else {
        month = dateNow.getMonth().toString();
    }

    if (dateNow.getDate() < 10) {
        day = "0" + dateNow.getDate();
    } else {
        day = dateNow.getDate().toString();
    }

    if (dateNow.getHours() < 10) {
        hour = "0" + (dateNow.getHours() + 1);
    } else {
        hour = dateNow.getHours().toString();
    }

    if (dateNow.getMinutes() < 10) {
        minets = "0" + (dateNow.getMinutes() + 1);
    } else {
        minets = dateNow.getMinutes().toString();
    }

    return (day + "." + month + "." + year + " - " + hour + ":" + minets + "\n");
}

/**
 * 
 * @param {String} number 
 * @returns {Number}
 */
function checkNumber(number) {
    let x = Number.parseInt(number);

    if (x == x && x > 0) {
        return x;
    } else {

        throw newError();
    }
}

/**
 * throw exception type Error with name ERTTRT
 * @returns {Error} ex
 */
function newError() {
    var ex = new Error("Это не число или число ниже нуля!");
    ex.name = "NotNumber";
    return ex;
}